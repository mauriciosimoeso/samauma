from django.urls import path, include
from rest_framework import routers

from books import views

default_router = routers.DefaultRouter()

default_router.register('books', views.BookViewSet, basename='books')
default_router.register('clients', views.ClientViewSet, basename='clients')


urlpatterns = [
    path('', include(default_router.urls))
]
