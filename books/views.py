from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, viewsets, status

# Create your views here.
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from books import models, serializers
from books.models import Client, Lending


class BookViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('USER_ID', type=openapi.TYPE_INTEGER, description="An user ID simulating a jwt token",
                          in_=openapi.IN_HEADER, required=True)
    ], request_body=serializers.LendActionSerializer)
    @action(detail=True, methods=['POST'], url_path="reserve")
    def lend(self, request, *args, **kwargs):
        if 'HTTP_USER_ID' not in request.META:
            raise ValidationError("You must inform the client's id")

        client = Client.objects.filter(id=int(request.META['HTTP_USER_ID'])).first()
        if not client:
            return Response(data={'error': 'Client not found'}, status=status.HTTP_404_NOT_FOUND)

        book = self.get_object()
        if book.lents.exists():
            raise ValidationError("This book is already lent")

        lending = Lending(client=client, book=book)
        lending.save()

        return Response(status=status.HTTP_202_ACCEPTED)


class ClientViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer

    @action(detail=True, url_path="books")
    def books(self, request, *arg, **kwargs):
        books = models.Book.objects.filter(lents__client__id=self.get_object().id)
        serializer = serializers.BookSerializer(books, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
