from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class Book(BaseModel):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "book"
        verbose_name_plural = "books"

    def __str__(self):
        return self.name


class Client(BaseModel):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "client"
        verbose_name_plural = "clients"

    def __str__(self):
        return self.name


class Lending(BaseModel):
    book = models.ForeignKey(to="Book", related_name="lents", on_delete=models.CASCADE, unique=True)
    client = models.ForeignKey(to="Client", related_name="lents", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "lend"
        verbose_name_plural = "lends"
