from datetime import timedelta

from django.utils.timezone import now
from rest_framework import serializers

from books import models


class LendingSerializer(serializers.Serializer):

    fee = serializers.SerializerMethodField('get_fee')
    interest = serializers.SerializerMethodField('get_interest')
    lend_date = serializers.DateTimeField(source='created_at')

    def get_fee(self, lending: models.Lending):
        if lending:
            if timedelta(days=3) + lending.created_at <= now() < timedelta(days=6) + lending.created_at:
                return .03
            if timedelta(days=6) + lending.created_at <= now() < timedelta(days=8) + lending.created_at:
                return .05
            if timedelta(days=8) + lending.created_at <= now():
                return .07

        return .0

    def get_interest(self, lending: models.Lending):
        if lending:
            if timedelta(days=3) + lending.created_at <= now() < timedelta(days=6) + lending.created_at:
                return .002
            if timedelta(days=6) + lending.created_at <= now() < timedelta(days=8) + lending.created_at:
                return .004
            if timedelta(days=8) + lending.created_at <= now():
                return .006
        return .0


class BookSerializer(serializers.ModelSerializer):

    lending_info = serializers.SerializerMethodField('get_lending_info')
    status = serializers.SerializerMethodField('get_status')

    class Meta:
        model = models.Book
        fields = '__all__'

    def get_status(self, book) -> str:
        return 'Emprestado' if book.lents.exists() else 'Disponível'

    def get_lending_info(self, book) -> LendingSerializer:
        return LendingSerializer(book.lents.first()).data if book.lents.exists() else None


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Client
        fields = '__all__'


class LendActionSerializer(serializers.Serializer):
    pass
