from django.contrib import admin

# Register your models here.
from books import models


@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Lending)
class LendAdmin(admin.ModelAdmin):
    pass