# Samauma

Um microserviço para simular o emprestimo de livros.

## Como executar

O projeto foi desenvolvido utilizando o docker-compose, então para executar a applicação 
basta executar o comando `docker-compose up app` na pasta raiz do projeto. Caso precise instalar
o docker-compose e docker aqui tem um [bom tutorial para o Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-pt).

## Como cadastrar dados

O projeto foi desenvolvido com um [admin](http://localhost:8000/admin) (usuário: `admin`, senha: `test`) onde é possível editar as 3 entidades `Book`, `Client` e `Lending`. A aplicação já conta com um banco de dados sqlite configurado, porém se por ventura 
isso não funcionar para você execute os seguintes passos:

 - Execute o comando `docker-compose up app`. Você deve ver a notificação no terminal que todas as migrações foram aplicadas com sucesso;
 - Execute o comando `docker-compose run --rm  app python manage.py createsuperuser`. Siga as instruções e você poderá acessar a administração da aplicação com o usuário e senha que gerou.

## Como executar as ações esperadas

Todas as APIs rest diponíveis podem ser vizualizadas no [swagger](http://localhost:8000/swagger). As ações podem ser executadas diretamente dessa interface. Entretanto, o cadastro dos dados deve ser feito através do admin.

### Dúvidas sobre o desafio

A descrição do desafio deixou alguns pontos bem vagos e como eu o desenvolvi no final de semana não achei prudente esperar por uma resposta. A seguir listo as dúvidas com o que eu assumi para o desenvolvimento:

 - Na frase "ao retornar os livros emprestados" da-se a entender que é esperada uma API para devolução. Entretanto, as demais foram identificadas explicitamente. Assumi que essa API não era para ser adicionada.
 - Na regra sobre o atraso, não está claro se os juros ao dia devem ser contabilizados de forma cumulativa (primeiros dias 0,2%, depois 0,3%, etc) ou mediante "apresentação do livro" a taxa será definida (Ex.: demorou 10 dias, logo será cobrado 0,6% ao dia). Assumi o segundo caso, por ser mais simples de desenvolver. A API irá apenas indicar quais são as taxas a serem cobradas no momento da devolução.