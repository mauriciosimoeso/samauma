from unittest.mock import ANY

import pytest
from django.test import Client
from freezegun import freeze_time
from rest_framework import status

from books.models import Book


@pytest.mark.usefixtures('more_books')
def test_list_books(client: Client):
    response = client.get('/api/v1/books/')

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 10


@pytest.mark.usefixtures('books')
def test_retrieve_book(client: Client):
    response = client.get('/api/v1/books/1/')

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['name'] == 'teste 1'


@pytest.mark.usefixtures('bookstore_client')
def test_retrieve_a_client(client: Client):
    response = client.get('/api/v1/clients/1/')
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['name'] == 'client 1'


@pytest.mark.usefixtures('bookstore_client', 'books')
def test_lend_a_book(client: Client):
    headers = {
        'HTTP_USER_ID': '1'
    }
    response = client.post('/api/v1/books/1/reserve/', content_type="application/json", data={}, **headers)

    assert response.status_code == status.HTTP_202_ACCEPTED

    lent_book = Book.objects.filter(id=1).get()
    assert lent_book.lents.exists()


@pytest.mark.usefixtures('bookstore_client', 'books', 'lending')
def test_lend_an_alread_lent_book(client: Client):
    # GIVEN
    headers = {
        'HTTP_USER_ID': '1'
    }
    # WHEN
    response = client.post('/api/v1/books/1/reserve/', content_type="application/json", data={}, **headers)

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.usefixtures('books', 'lending')
def test_client_lent_books_list(client: Client, bookstore_client):
    response = client.get(f'/api/v1/clients/{bookstore_client.id}/books/')

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1
    assert response.json() == [{
        'id': 1,
        'name': 'teste 1',
        'created_at': ANY,
        'updated_at': ANY,
        'status': 'Emprestado',
        'lending_info': {
            'fee': 0.0,
            'interest': 0.0,
            'lend_date': ANY
        }
    }]


@pytest.mark.parametrize('date,expected_fee,expected_interest', [
    ('2021-01-02', 0.0, 0.0),
    ('2021-01-04', 0.03, 0.002),
    ('2021-01-07', 0.05, 0.004),
    ('2021-01-09', 0.07, 0.006),
])
@pytest.mark.usefixtures('books', 'lending_january_first')
def test_client_lent_books_list_late(client: Client, bookstore_client, date, expected_fee, expected_interest):
    with freeze_time(date):
        response = client.get(f'/api/v1/clients/{bookstore_client.id}/books/')

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1
    assert response.json() == [{
        'id': 1,
        'name': 'teste 1',
        'created_at': ANY,
        'updated_at': ANY,
        'status': 'Emprestado',
        'lending_info': {
            'fee': expected_fee,
            'interest': expected_interest,
            'lend_date': ANY
        }
    }]
