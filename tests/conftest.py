import pytest
from django.utils.timezone import now
from freezegun import freeze_time

from books.models import Book, Client, Lending


@pytest.fixture
def books(db):
    book = Book(name="teste 1")
    book.save()
    return book


@pytest.fixture
def more_books(db):
    books = [Book(name=f'Book {i}') for i in range(10)]
    for b in books:
        b.save()

    return books


@pytest.fixture
def bookstore_client(db):
    client = Client(name="client 1")
    client.save()
    return client


@pytest.fixture
def lending(db, books, bookstore_client):
    lending = Lending(book=books, client=bookstore_client)
    lending.save()
    return lending


@pytest.fixture
def lending_january_first(db, books, bookstore_client):
    with freeze_time('2021-01-01'):
        lending = Lending(book=books, client=bookstore_client, created_at=now(), updated_at=now())
        lending.save()
    return lending
